## pytoxns

Tox DNS as a Python module, i.e. "what if [this](https://github.com/Tox/Tox-QuickDNS)
was more flexible".

    pip install pytoxns

## Dependencies

- Python 3.x
- PyNaCl
- dnslib

Please install them from PyPI. (`pip install -r requirements.txt`)

## Configuration

> *心配しないで。*  (TL note: "Don't worry")

Configuration is hundreds of times easier than getting a mail server
to work.

### At your DNS host

You need to add a NS record pointing to your server for
`_tox.[your domain here]`.

It would look like this in BIND format.
`_tox	3600		IN	NS	cerberus.zodiaclabs.org.`

## Usage

    from pytoxns import make_server

    server, pubkey = make_server(lambda friendly_id: "tox_id", "your.hostname.com", port=56, listen="1.2.3.4")
    print(pubkey)
    server.start()

You need to be root to bind to port 53. I suggest you setuid before calling `server.start` to drop perms.

Your lookup method will be passed the full key (`sircmpwn@sr.ht` for example) and you should return the Tox ID or None.

## Why did you fork this

Cause I wanted to integrate tox IDs with an existing account system I have, and supplying a JSON file is stupid.
